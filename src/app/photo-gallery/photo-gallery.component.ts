import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.css']
})
export class PhotoGalleryComponent implements OnInit {

  randomPhotos = ['https://www.hellomagazine.com/imagenes/profiles/sandra-bullock/5777-sandra-bullock.jpg',
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/JenniferAnistonHWoFFeb2012.jpg/800px-JenniferAnistonHWoFFeb2012.jpg',
        'https://www.thesun.co.uk/wp-content/uploads/2017/06/nintchdbpict000283678777.jpg?strip=all&w=640',
        'https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_80%2Cw_300/MTE1ODA0OTcxOTA3NTgxNDUz/hugh-jackman-16599916-1-402.jpg'];

  randomPictureResult = '';
  celebritiesName = ['Sandra' , 'Jeniffer', 'Brad', 'Hugh'];   
  
  trackLikes = [0,0,0,0];

  name = 'Nameless';
  constructor() { }
  noOfDislike = 0;
  noOflike = 0;
  outcome = 0;

  ngOnInit() {
    this.randomizePhoto();
  }

  randomizePhoto(){
    var outcome =  Math.floor(Math.random() * Math.floor(4));
    this.randomPictureResult = this.randomPhotos[Math.ceil(outcome)];
    this.name = this.celebritiesName[outcome];
    var idxCurrentCeleb = this.celebritiesName.indexOf(this.name);
    if(this.trackLikes[idxCurrentCeleb] == 3){
      if(this.hasDuplicates(this.trackLikes)){
        this.trackLikes = [0,0,0,0];
      }
      this.randomizePhoto();
    }
  }

  onDisLikeEvt(event){
    this.noOfDislike++;
    this.randomizePhoto();
  }

  hasDuplicates(a: any) {
    return _.uniq(a).length !== a.length; 
  }

  onLikeEvt(event){
    var indexOfTheCelebrity = this.celebritiesName.indexOf(this.name);
    this.noOflike++;
    if(this.trackLikes[indexOfTheCelebrity] != 3){
      this.trackLikes[indexOfTheCelebrity] = 
        this.trackLikes[indexOfTheCelebrity] + 1;
    }
    
    this.randomizePhoto();
  }

}
