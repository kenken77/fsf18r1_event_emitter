import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dislike-me',
  templateUrl: './dislike-me.component.html',
  styleUrls: ['./dislike-me.component.css']
})
export class DislikeMeComponent implements OnInit {
  @Output() onDisLikeEvt = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  dislike(likeFlag: boolean){
    console.log(likeFlag);
    this.onDisLikeEvt.emit(likeFlag);
  }

}
