import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DislikeMeComponent } from './dislike-me.component';

describe('DislikeMeComponent', () => {
  let component: DislikeMeComponent;
  let fixture: ComponentFixture<DislikeMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DislikeMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DislikeMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
