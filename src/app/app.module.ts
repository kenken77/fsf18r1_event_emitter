import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { PhotoGalleryComponent } from './photo-gallery/photo-gallery.component';
import { LikeMeComponent } from './like-me/like-me.component';
import { DislikeMeComponent } from './dislike-me/dislike-me.component';


@NgModule({
  declarations: [
    AppComponent,
    PhotoGalleryComponent,
    LikeMeComponent,
    DislikeMeComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
