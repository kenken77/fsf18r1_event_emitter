import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-like-me',
  templateUrl: './like-me.component.html',
  styleUrls: ['./like-me.component.css']
})
export class LikeMeComponent implements OnInit {
  @Output() onLikeEvt = new EventEmitter<boolean>();
  
  constructor() { }

  ngOnInit() {
  }

  like(likeFlag: boolean){
    this.onLikeEvt.emit(likeFlag);
  }

}
