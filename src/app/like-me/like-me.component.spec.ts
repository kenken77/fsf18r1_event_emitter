import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LikeMeComponent } from './like-me.component';

describe('LikeMeComponent', () => {
  let component: LikeMeComponent;
  let fixture: ComponentFixture<LikeMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LikeMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikeMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
